#ifndef __FID_H__
#define __FID_H__

#include <c9-server.h>
#include <stdint.h>

struct fid_struct {
	uint32_t fid;
	/* Points to relevant tree element */
	const struct C9p_tree_el *te;
	/* Points to relevant association */
	const struct C9p_association *association;
	/* Current file pointer */
	unsigned long seekptr;
	/* For internal use */
	struct list_head list;
};

extern int add_fid(struct C9p_connection *c, uint32_t fid,
		   const struct C9p_tree_el *e,
		   const struct C9p_association *a);

extern struct fid_struct *find_fid(struct C9p_connection *c, uint32_t fid);

extern int clunk_fid(struct C9p_connection *c, uint32_t fid);

#endif /* __FID_H__ */

