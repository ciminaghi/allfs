#ifndef __C9_SERVER_H__
#define __C9_SERVER_H__

#include <c9-proto.h>

#ifndef FIDS_TABLE_SIZE
#define FIDS_TABLE_SIZE 64
#endif

#ifndef BUF_SIE
#define BUF_SIZE 1024
#endif

/* Max dir entries in a directory */
#ifndef MAX_DENTRIES
#define MAX_DENTRIES 32
#endif

struct C9p_connection {
	struct C9p_connection_host_data *data;
	struct list_head associations;
	struct list_head busy_buffers;
	struct list_head list;
	struct list_head fids_table[FIDS_TABLE_SIZE];
};

struct C9p_tree_el;

struct C9p_association {
	C9fid fid;
	C9fid afid;
	const struct C9p_tree_el *root;
	char *uname;
	struct list_head list;
	uint8_t buf[BUF_SIZE];
};

struct C9p_fops {
	int (*open)(const struct C9p_tree_el *, uint8_t mode);
	int (*stat)(const struct C9p_tree_el *, struct C9stat *);
	int (*read)(const struct C9p_tree_el *, uint64_t offset,
		    uint32_t count, uint8_t *data);
	int (*write)(const struct C9p_tree_el *, uint64_t offset,
		     uint32_t count, const uint8_t *data);
	int (*release)(const struct C9p_tree_el *);
	/* Mandatory */
	uint64_t (*get_path)(const struct C9p_tree_el *e);
};

struct C9p_children_data {
	int max_nchildren;
	int nchildren;
	const struct C9p_tree_el **children;
};

struct C9p_tree_el_data {
	int refcnt;
	uint32_t version;
	/* add access time here */
	/* .... */
	/* Points to runtime added children */
	struct C9p_children_data *runtime_children_data;
};

struct C9p_tree_el {
	const struct C9p_tree_el *parent;
	struct C9p_tree_el_data *data;
	const struct C9stat *stat;
	/* Statically instantiated children */
	const struct C9p_children_data *static_children_data;
	const struct C9p_fops *fops;
	const void *priv;
};

#define declare_tree_el(e,p,sz,_uid,_gid,t,_path,_mode,d,scd,fo,pr)	\
	static const struct C9stat xcat(e,_stat) = {			\
		.size = sz,						\
		.name = xstr(e),					\
		.uid = _uid,						\
		.gid = _gid,						\
		.muid = "",						\
		.qid = {						\
			.type = t,					\
			.path = _path,					\
		},							\
		.mode = _mode,						\
	};								\
	static const struct C9p_tree_el e = {				\
		.parent = p,						\
		.stat = &xcat(e,_stat),					\
		.data = d,						\
		.static_children_data = scd,				\
		.fops = fo,						\
		.priv = pr,						\
	};

#define declare_root_el(r,_uid,_gid,t,_path,_mode,d,scd,fo,pr)		\
	static const struct C9stat xcat(r,_stat) = {			\
		.size = 0,						\
		.name = xstr(r),					\
		.uid = _uid,						\
		.gid = _gid,						\
		.muid = "",						\
		.qid = {						\
			.type = t,					\
			.path = _path,					\
		},							\
		.mode = _mode,						\
	};								\
	static const struct C9p_tree_el r				\
	__attribute__((section("9proots"),used)) = {			\
		.parent = &r,						\
		.stat = &xcat(r,_stat),					\
		.data = d,						\
		.static_children_data = scd,				\
		.fops = fo,						\
		.priv = pr,						\
	};


extern struct C9p_connection *
new_connection(struct C9p_connection_host_data *data);
extern struct C9p_association *new_association(struct C9p_connection *c,
					       C9fid fid, C9fid afid,
					       const char *aname,
					       const char *uname);
extern void free_connection(struct C9p_connection *);
extern void free_association(struct C9p_association *);

extern int default_end(C9ctx *ctx);
extern uint8_t *default_begin(C9ctx *ctx, uint32_t size);
extern void default_t(C9ctx *ctx, C9t *t);

extern int dir_read(const struct C9p_tree_el *e, uint64_t offset,
		    uint32_t count, uint8_t *data);
extern int dir_stat(const struct C9p_tree_el *e, struct C9stat *s);

extern struct C9p_tree_el roots_start[], roots_end[];

#endif /* __C9_SERVER_H__ */
