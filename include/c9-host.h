#ifndef __C9_HOST_H__
#define __C9_HOST_H__

#ifndef HOST
#define HOST_linux
#endif

#ifdef HOST_esp8266
#include "c9-esp8266.h"
#elif defined HOST_linux
#include "c9-linux.h"
#else
#error "HOST is not defined"
#endif

extern int c9_send(struct C9p_connection_host_data *, const void *buf,
		   unsigned long sz);

#endif /* __C9_HOST_H__ */
