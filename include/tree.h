#ifndef __TREE_H__
#define __TREE_H__

#include "c9-server.h"

extern const struct C9p_tree_el *find_root(const char *name);
extern const struct C9p_tree_el *traverse(struct C9p_connection *c,
					  uint32_t start_fid,
					  char *wname[],
					  int *nqs,
					  struct C9qid qs[],
					  struct C9p_association **a);

extern int generic_stat(const struct C9p_tree_el *e, struct C9stat *s);


#endif /* __TREE_H__ */

