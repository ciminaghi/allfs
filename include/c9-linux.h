#ifndef __C9_LINUX_H__
#define __C9_LINUX_H__

#include <stdint.h>
#include <stdio.h>
#include <sys/time.h>

struct C9p_connection_host_data {
	int fd;
};


static inline unsigned get_time(void)
{
	struct timeval tv;

	gettimeofday(&tv, NULL);
	return tv.tv_sec * 1000000 + tv.tv_usec;
}

#define c9_log(a,args...) fprintf(stderr, "[%08u] C9: " a, get_time(), ##args)
#define c9_err(a,args...) fprintf(stderr, "[%08u] C9 ERROR: " a, get_time(), \
				  ##args)
#define c9_log_noprefix(a,args...) fprintf(stderr, a, ##args)

#include <string.h>

#endif /* __C9_LINUX_H__ */
