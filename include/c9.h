#ifndef __C9_H__
#define __C9_H__

#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))
#define DIV_ROUND_UP(n,d) (((n) + (d) - 1) / (d))
#ifndef offsetof
#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)
#endif
#define container_of(ptr, type, member) ({			\
	const typeof( ((type *)0)->member ) *__mptr = (ptr);	\
	(type *)( (char *)__mptr - offsetof(type,member) );})
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))
#define cat(a,b) a##b
#define xcat(a,b) cat(a,b)
#define str(a) #a
#define xstr(a) str(a)

#include "c9-host.h"

#endif /* __C9_H__ */

