#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>
#include <c9.h>
#include <fid.h>
#include <list.h>
#include <tree.h>

/* Setup a very simple server talking through stdin/stdout */

static uint8_t *_linux_read(C9ctx *ctx, uint32_t size, int *err)
{
	static uint8_t buf[4096];
	uint32_t done;
	int stat;

	fprintf(stderr, "%s %d\n", __func__, __LINE__);
	if (size > sizeof(buf)) {
		fprintf(stderr, "%s: unsupported size %u\n", __func__, size);
		*err = EINVAL;
	}

	for (done = 0, *err = 0; done < size; done += stat) {
		stat = read(0, buf, size);
		if (stat < 0) {
			perror("read");
			*err = errno;
			return NULL;
		}
	}
	fprintf(stderr, "%s returns %p (size = %u)\n", __func__, buf, size);
	return buf;
}

static void _linux_r(C9ctx *ctx, C9r *t)
{
	fprintf(stderr, "%s invoked, t->type = %d\n", __func__, t->type);
}

static void _linux_err(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
}

static int rx_open(const struct C9p_tree_el *e, uint8_t mode)
{
	c9_log("%s, mode = %d\n", __func__, mode);
	return 0;
}

static int rx_write(const struct C9p_tree_el *e, uint64_t offset,
		    uint32_t count, const uint8_t *data)
{
	c9_log("%s, offset = %lu, count = %u\n", __func__, offset, count);
	c9_log("data = %s\n", (char *)data);
	return count;
}

static int rx_release(const struct C9p_tree_el *e)
{
	c9_log("%s\n", __func__);
	return 0;
}

static uint64_t rx_get_path(const struct C9p_tree_el *e)
{
	return 2ULL;
}

static const struct C9p_fops rx_fops = {
	.open = rx_open,
	.stat = generic_stat,
	.write = rx_write,
	.release = rx_release,
	.get_path = rx_get_path,
};



static int tx_open(const struct C9p_tree_el *e, uint8_t mode)
{
	c9_log("%s, mode = %d\n", __func__, mode);
	return 0;
}

static int tx_read(const struct C9p_tree_el *e, uint64_t offset, uint32_t count,
		   uint8_t *data)
{
	c9_log("%s, offset = %lu, count = %u\n", __func__, offset, count);
	if (offset >= strlen("hello world\n"))
		return 0;
	strncpy((char *)data, "hello world\n", count);
	return strlen((char *)data);
}

static int tx_release(const struct C9p_tree_el *e)
{
	c9_log("%s\n", __func__);
	return 0;
}

static uint64_t tx_get_path(const struct C9p_tree_el *e)
{
	return 3ULL;
}

static const struct C9p_fops tx_fops = {
	.open = tx_open,
	.stat = generic_stat,
	.read = tx_read,
	.release = tx_release,
	.get_path = tx_get_path,
};


static const struct C9p_tree_el thread;

static struct C9p_tree_el_data thread_rx_data;

declare_tree_el(thread_rx, &thread, 0, "ciminaghi", "ciminaghi",
		C9qtfile, 100ULL, C9permuw|C9permow|C9permgw,
		&thread_rx_data, NULL,
		&rx_fops, NULL);

static struct C9p_tree_el_data thread_tx_data;

declare_tree_el(thread_tx, &thread, 0, "ciminaghi", "ciminaghi",
		C9qtfile, 200ULL, C9permur|C9permor|C9permgr,
		&thread_tx_data, NULL,
		&tx_fops, NULL);


static const struct C9p_tree_el *thread_children[] = {
	&thread_rx,
	&thread_tx,
};

static struct C9p_children_data thread_static_children_data = {
	.max_nchildren = 2,
	.nchildren = 2,
	.children = thread_children,
};

static struct C9p_tree_el_data thread_data;

static uint64_t thread_get_path(const struct C9p_tree_el *e)
{
	return 0ULL;
}

static const struct C9p_fops thread_fops = {
	.read = dir_read,
	.stat = generic_stat,
	.get_path = thread_get_path,
};

#define permall C9permur|C9permux|C9permuw|C9permgr|C9permgx|C9permgw| \
    C9permor|C9permox|C9permow

declare_root_el(thread, "ciminaghi", "ciminaghi", C9qtdir, 1234ULL,
		permall|C9stdir,
		&thread_data, &thread_static_children_data, &thread_fops, NULL);

/* TEMPORARILY DEFINED HERE */
int c9_send(struct C9p_connection_host_data *cd, const void *buf,
	    unsigned long sz)
{
	c9_log("%s, cd = %p, %p, %lu\n", __func__, cd, buf, sz);
	return write(cd->fd, buf, sz);
}

int main(int argc, char *argv[])
{
	struct C9p_connection_host_data cd = {
		.fd = 1,
	};
	struct C9ctx ctx = {
		.read = _linux_read,
		.begin = default_begin,
		.end = default_end,
		.r = _linux_r,
		.t = default_t,
		.error = _linux_err,
		.msize = C9minmsize,
	};

	ctx.aux = new_connection(&cd);

	while(!s9proc(&ctx));

	return 0;
}
