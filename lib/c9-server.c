/*
 * 9p server implementation, see https://git.sr.ht/~ft/c9
 */
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include "c9-proto.h"

static uint8_t *
R(C9ctx *c, uint32_t size, C9rtype type, C9tag tag, C9error *err)
{
	uint8_t *p = NULL;

	if(size > c->msize-4-1-2){
		c->error("R: invalid size");
		*err = C9Esize;
	}else{
		size += 4+1+2;
		if((p = c->begin(c, size)) == NULL){
			c->error("R: no buffer");
			*err = C9Ebuf;
		}else{
			*err = 0;
			w32(&p, size);
			w08(&p, type);
			w16(&p, tag);
		}
	}
	return p;
}

C9error
s9version(C9ctx *c)
{
	uint8_t *b;
	C9error err;

	if((b = R(c, 4+2+6, Rversion, 0xffff, &err)) != NULL){
		w32(&b, c->msize);
		wcs(&b, "9P2000", 6);
		err = c->end(c);
	};
	return err;
}

C9error
s9auth(C9ctx *c, C9tag tag, const C9qid *aqid)
{
	uint8_t *b;
	C9error err;

	if((b = R(c, 13, Rauth, tag, &err)) != NULL){
		w08(&b, aqid->type);
		w32(&b, aqid->version);
		w64(&b, aqid->path);
		err = c->end(c);
	}
	return err;
}

C9error
s9error(C9ctx *c, C9tag tag, const char *ename)
{
	uint32_t len = safestrlen(ename);
	uint8_t *b;
	C9error err;

	c9_log("%s, len = %u\n", __func__, len);
	if(len > C9maxstr){
		c->error("s9error: invalid ename");
		return C9Estr;
	}
	if((b = R(c, 2+len, Rerror, tag, &err)) != NULL){
		wcs(&b, ename, len);
		err = c->end(c);
	}
	return err;
}

C9error
s9attach(C9ctx *c, C9tag tag, const C9qid *qid)
{
	uint8_t *b;
	C9error err;

	if((b = R(c, 13, Rattach, tag, &err)) != NULL){
		w08(&b, qid->type);
		w32(&b, qid->version);
		w64(&b, qid->path);
		err = c->end(c);
	}
	return err;
}

C9error
s9flush(C9ctx *c, C9tag tag)
{
	uint8_t *b;
	C9error err;

	if((b = R(c, 0, Rflush, tag, &err)) != NULL)
		err = c->end(c);
	return err;
}

C9error
s9walk(C9ctx *c, C9tag tag, int nqids, const C9qid qids[])
{
	int i, n = nqids;
	uint8_t *b;
	C9error err;

	if(n > C9maxpathel){
		c->error("s9walk: invalid elements !(0 <= %d <= %d)", n, C9maxpathel);
		return C9Epath;
	}

	if((b = R(c, 2+n*13, Rwalk, tag, &err)) != NULL){
		w16(&b, n);
		for(i = 0; i < n; i++){
			w08(&b, qids[i].type);
			w32(&b, qids[i].version);
			w64(&b, qids[i].path);
		}
		err = c->end(c);
	}
	return err;
}

C9error
s9open(C9ctx *c, C9tag tag, const C9qid *qid, uint32_t iounit)
{
	uint8_t *b;
	C9error err;

	if((b = R(c, 13+4, Ropen, tag, &err)) != NULL){
		w08(&b, qid->type);
		w32(&b, qid->version);
		w64(&b, qid->path);
		w32(&b, iounit);
		err = c->end(c);
	}
	return err;
}

C9error
s9create(C9ctx *c, C9tag tag, const C9qid *qid, uint32_t iounit)
{
	uint8_t *b;
	C9error err;

	if((b = R(c, 13+4, Rcreate, tag, &err)) != NULL){
		w08(&b, qid->type);
		w32(&b, qid->version);
		w64(&b, qid->path);
		w32(&b, iounit);
		err = c->end(c);
	}
	return err;
}

C9error
s9read(C9ctx *c, C9tag tag, const void *data, uint32_t size)
{
	uint8_t *b;
	C9error err;

	if((b = R(c, 4+size, Rread, tag, &err)) != NULL){
		w32(&b, size);
		memmove(b, data, size);
		err = c->end(c);
	}
	return err < 0 ? err : size;
}

C9error
s9write(C9ctx *c, C9tag tag, uint32_t size)
{
	uint8_t *b;
	C9error err;

	if((b = R(c, 4, Rwrite, tag, &err)) != NULL){
		w32(&b, size);
		err = c->end(c);
	}
	return err;
}

C9error
s9readdir(C9ctx *c, C9tag tag, const C9stat *st[], int *num, uint64_t *offset, uint32_t size)
{
	uint8_t *b;
	const C9stat *s;
	uint32_t nlen, ulen, glen, mulen, m, n;
	C9error err;
	int i;

	c9_log("%s, size = %u, c->msize = %u\n", __func__, size, c->msize);
	if(size > c->msize-4-1-2)
		size = c->msize-4-1-2;
	c9_log("%s, size = %u, num = %d\n", __func__, size, *num);

	m = 0;
	for(i = 0; i < *num; i++){
		s = st[i];
		nlen = safestrlen(s->name);
		ulen = safestrlen(s->uid);
		glen = safestrlen(s->gid);
		mulen = safestrlen(s->muid);

		if(nlen == 0 || nlen > C9maxstr){
			c->error("s9readdir: invalid name");
			return C9Epath;
		}
		if(ulen > C9maxstr || glen > C9maxstr || mulen > C9maxstr){
			c->error("s9readdir: string too long");
			return C9Estr;
		}

		n = 2 + 2+4+13+4+4+4+8+2+nlen+2+ulen+2+glen+2+mulen;
		c9_log("nlen = %" PRIu32 ", ulen = %u, glen = %u, mulen = %u, n = %u\n",
		       nlen, ulen, glen, mulen, n);
		/* FIXME: WHAT'S THIS ? */
		/* if(4+m+n > size)
			break;
		*/
		m += n;
		c9_log("m = %d\n", m);
	}

	if((b = R(c, 4+m, Rread, tag, &err)) != NULL){
		*num = i;
		w32(&b, m);
		for(i = 0; i < *num; i++){
			s = st[i];
			nlen = safestrlen(s->name);
			ulen = safestrlen(s->uid);
			glen = safestrlen(s->gid);
			mulen = safestrlen(s->muid);
			w16(&b, 2+4+13+4+4+4+8+2+nlen+2+ulen+2+glen+2+mulen);
			w16(&b, 0xffff); /* type */
			w32(&b, 0xffffffff); /* dev */
			w08(&b, s->qid.type);
			w32(&b, s->qid.version);
			w64(&b, s->qid.path);
			w32(&b, s->mode);
			w32(&b, s->atime);
			w32(&b, s->mtime);
			w64(&b, s->size);
			wcs(&b, s->name, nlen);
			wcs(&b, s->uid, ulen);
			wcs(&b, s->gid, glen);
			wcs(&b, s->muid, mulen);
		}
		err = c->end(c);
		if(err == 0)
			*offset += m;
	}
	return err;
}

C9error
s9clunk(C9ctx *c, C9tag tag)
{
	uint8_t *b;
	C9error err;

	if((b = R(c, 0, Rclunk, tag, &err)) != NULL)
		err = c->end(c);
	return err;
}

C9error
s9remove(C9ctx *c, C9tag tag)
{
	uint8_t *b;
	C9error err;

	if((b = R(c, 0, Rremove, tag, &err)) != NULL)
		err = c->end(c);
	return err;
}

C9error
s9stat(C9ctx *c, C9tag tag, const C9stat *s)
{
	uint32_t nlen = safestrlen(s->name), ulen = safestrlen(s->uid);
	uint32_t glen = safestrlen(s->gid), mulen = safestrlen(s->name);
	uint32_t statsz = 2+4+13+4+4+4+8+2+nlen+2+ulen+2+glen+2+mulen;
	uint8_t *b;
	C9error err;

	if(nlen == 0 || nlen > C9maxstr){
		c->error("s9stat: invalid name");
		return C9Epath;
	}
	if(ulen > C9maxstr || glen > C9maxstr || mulen > C9maxstr){
		c->error("s9stat: string too long");
		return C9Estr;
	}

	if((b = R(c, 2+2+statsz, Rstat, tag, &err)) != NULL){
		w16(&b, statsz+2);
		w16(&b, statsz);
		w16(&b, 0xffff); /* type */
		w32(&b, 0xffffffff); /* dev */
		w08(&b, s->qid.type);
		w32(&b, s->qid.version);
		w64(&b, s->qid.path);
		w32(&b, s->mode);
		w32(&b, s->atime);
		w32(&b, s->mtime);
		w64(&b, s->size);
		wcs(&b, s->name, nlen);
		wcs(&b, s->uid, ulen);
		wcs(&b, s->gid, glen);
		wcs(&b, s->muid, mulen);
		err = c->end(c);
	}
	return err;
}

C9error
s9wstat(C9ctx *c, C9tag tag)
{
	uint8_t *b;
	C9error err;

	if((b = R(c, 0, Rwstat, tag, &err)) != NULL)
		err = c->end(c);
	return err;
}

C9error
s9proc(C9ctx *c)
{
	uint32_t i, sz, cnt, n, msize;
	int readerr;
	uint8_t *b;
	C9error err;
	C9t t;

	readerr = -1;
	if((b = c->read(c, 4, &readerr)) == NULL){
		if(readerr != 0)
			c->error("s9proc: short read");
		return readerr == 0 ? 0 : C9Epkt;
	}

	sz = r32(&b);
	if(sz < 7 || sz > c->msize){
		c->error("s9proc: invalid packet size !(7 <= %u <= %u)",
			 sz, c->msize);
		return C9Epkt;
	}
	sz -= 4;
	readerr = -1;
	if((b = c->read(c, sz, &readerr)) == NULL){
		if(readerr != 0)
			c->error("s9proc: short read");
		return readerr == 0 ? 0 : C9Epkt;
	}

	t.type = r08(&b);
	t.tag = r16(&b);
	sz -= 3;

	if((c->svflags & Svver) == 0 && t.type != Tversion){
		c->error("s9proc: expected Tversion, got %d", t.type);
		return C9Epkt;
	}

	switch(t.type){
	case Tread:
		if(sz < 4+8+4)
			goto error;
		t.fid = r32(&b);
		t.read.offset = r64(&b);
		t.read.size = r32(&b);
		if(t.read.size > maxread(c))
		  t.read.size = maxread(c);
		c->t(c, &t);
		break;

	case Twrite:
		if(sz < 4+8+4)
			goto error;
		t.fid = r32(&b);
		t.write.offset = r64(&b);
		if((t.write.size = r32(&b)) < sz-4-8-4)
			goto error;
		if(t.write.size > maxwrite(c))
		  t.write.size = maxwrite(c);
		t.write.data = b;
		c->t(c, &t);
		break;

	case Tclunk:
	case Tstat:
	case Tremove:
		if(sz < 4)
			goto error;
		t.fid = r32(&b);
		c->t(c, &t);
		break;

	case Twalk:
		if(sz < 4+4+2)
			goto error;
		t.fid = r32(&b);
		t.walk.newfid = r32(&b);
		if((n = r16(&b)) > 16){
			c->error("s9proc: Twalk !(%d <= 16)", n);
			return C9Epath;
		}
		sz -= 4+4+2;
		if(n > 0){
			for(i = 0; i < n; i++){
				if(sz < 2 || (cnt = r16(&b)) > sz-2)
					goto error;
				if(cnt < 1){
					c->error("s9proc: Twalk invalid element [%d]", i);
					return C9Epath;
				}
				b[-2] = 0;
				t.walk.wname[i] = (char*)b;
				b += cnt;
				sz -= 2 + cnt;
			}
			memmove(t.walk.wname[i-1]-1, t.walk.wname[i-1], (char*)b - t.walk.wname[i-1]);
			t.walk.wname[i-1]--;
			b[-1] = 0;
		}else
			i = 0;
		t.walk.wname[i] = NULL;
		c->t(c, &t);
		break;

	case Topen:
		if(sz < 4+1)
			goto error;
		t.fid = r32(&b);
		t.open.mode = r08(&b);
		c->t(c, &t);
		break;

	case Twstat:
		if(sz < 4+2)
			goto error;
		t.fid = r32(&b);
		if((cnt = r16(&b)) > sz-4)
			goto error;
		if((err = c9parsedir(c, &t.wstat, &b, &cnt)) != 0){
			c->error("s9proc");
			return err;
		}
		c->t(c, &t);
		break;

	case Tcreate:
		if(sz < 4+2+4+1)
			goto error;
		t.fid = r32(&b);
		if((cnt = r16(&b)) < 1 || cnt > sz-4-2-4-1)
			goto error;
		t.create.name = (char*)b;
		t.create.perm = r32(&b);
		t.create.mode = r08(&b);
		t.create.name[cnt] = 0;
		c->t(c, &t);
		break;

	case Tflush:
		if(sz < 2)
			goto error;
		t.flush.oldtag = r16(&b);
		c->t(c, &t);
		break;

	case Tversion:
		if(sz < 4+2 || (msize = r32(&b)) < C9minmsize || (cnt = r16(&b)) > sz-4-2)
			goto error;
		if(cnt < 6 || memcmp(b, "9P2000", 6) != 0){
			if((b = R(c, 4+2+7, Rversion, 0xffff, &err)) != NULL){
				w32(&b, 0);
				wcs(&b, "unknown", 7);
				err = c->end(c);
				c->error("s9proc: invalid version");
			}
			return C9Ever;
		}
		if(msize < c->msize)
			c->msize = msize;
		c->svflags |= Svver;
		c->t(c, &t);
		break;

	case Tattach:
		if(sz < 4+4+2+2)
			goto error;
		t.fid = r32(&b);
		t.attach.afid = r32(&b);
		cnt = r16(&b);
		sz -= 4+4+2;
		if(cnt+2 > sz)
			goto error;
		t.attach.uname = (char*)b;
		b += cnt;
		cnt = r16(&b);
		b[-2] = 0;
		sz -= cnt+2;
		if(cnt > sz)
			goto error;
		memmove(b-1, b, cnt);
		t.attach.aname = (char*)b-1;
		t.attach.aname[cnt] = 0;
		c->t(c, &t);
		break;

	case Tauth:
		if(sz < 4+2+2)
			goto error;
		t.auth.afid = r32(&b);
		cnt = r16(&b);
		sz -= 4+2;
		if(cnt+2 > sz)
			goto error;
		t.auth.uname = (char*)b;
		b += cnt;
		cnt = r16(&b);
		b[-2] = 0;
		sz -= cnt+2;
		if(cnt > sz)
			goto error;
		memmove(b-1, b, cnt);
		t.auth.aname = (char*)b-1;
		t.auth.aname[cnt] = 0;
		c->t(c, &t);
		break;

	default:
		goto error;
	}
	return 0;
error:
	c->error("s9proc: invalid packet (type=%d)", t.type);
	return C9Epkt;
}
