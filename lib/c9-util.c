#include <string.h>
#include <list.h>
#include <c9.h>
#include <c9-server.h>
#include <tree.h>
#include <fid.h>

#ifndef MAX_ASSOCIATIONS
#define MAX_ASSOCIATIONS 8
#endif
#ifndef MAX_CONNECTIONS
#define MAX_CONNECTIONS 8
#endif

#ifndef MAX_BUFFER_SIZE
#define MAX_BUFFER_SIZE 1024
#endif

#ifndef NBUFFERS
#define NBUFFERS 8
#endif

struct C9p_buffer {
	uint8_t data[MAX_BUFFER_SIZE];
	struct list_head list;
	int size;
};


static struct C9p_association associations_array[MAX_ASSOCIATIONS];
static struct C9p_connection connections_array[MAX_CONNECTIONS];
static struct C9p_buffer buffers_array[NBUFFERS];

static struct list_head free_associations;
static struct list_head free_connections;
static struct list_head free_buffers;

static int _init(void)
{
	int i;

	INIT_LIST_HEAD(&free_associations);
	INIT_LIST_HEAD(&free_connections);
	INIT_LIST_HEAD(&free_buffers);
	for (i = 0; i < MAX_ASSOCIATIONS; i++)
		list_add(&associations_array[i].list, &free_associations);
	for (i = 0; i < MAX_CONNECTIONS; i++)
		list_add(&connections_array[i].list, &free_connections);
	for (i = 0; i < NBUFFERS; i++)
		list_add(&buffers_array[i].list, &free_buffers);
	return 0;
}

static int _maybe_init(void)
{
	static int initialized;

	if (initialized)
		return 0;
	if (_init() < 0)
		return -1;
	initialized = 1;
	return 0;
}

struct C9p_connection *new_connection(struct C9p_connection_host_data *data)
{
	struct C9p_connection *out = NULL;
	
	if (_maybe_init() < 0)
		return out;
	if (list_empty(&free_connections))
		return out;
	out = list_entry(free_connections.next, typeof(*out), list);
	list_del(&out->list);
	out->data = data;
	INIT_LIST_HEAD(&out->associations);
	INIT_LIST_HEAD(&out->busy_buffers);
	memset(&out->fids_table, 0, sizeof(out->fids_table));
	return out;
}

struct C9p_association *new_association(struct C9p_connection *c,
					C9fid fid, C9fid afid,
					const char *aname,
					const char *uname)
{
	struct C9p_association *out = NULL;
	const struct C9p_tree_el *r;
	
	if (_maybe_init() < 0)
		return out;
	if (list_empty(&free_associations))
		return out;
	r = find_root(aname);
	if (!r)
		return out;
	out = list_entry(free_associations.next, typeof(*out), list);
	out->fid = fid;
	out->afid = afid;
	out->root = r;
	out->uname = strdup(uname);
	if (add_fid(c, fid, r, out) < 0)
		return NULL;
	c9_log("%s %d\n", __func__, __LINE__);
	list_move_tail(&out->list, &c->associations);
	c9_log("%s %d, out = %p\n", __func__, __LINE__, out);
	return out;
}

static struct C9p_buffer *get_buffer(struct C9p_connection *c, uint32_t size)
{
	struct C9p_buffer *out = NULL;

	if (!c)
		return out;
	if (size > MAX_BUFFER_SIZE)
		return out;
	if (_maybe_init() < 0)
		return out;
	if (list_empty(&free_buffers))
		return out;
	out = list_first_entry(&free_buffers, struct C9p_buffer, list);
	out->size = size;
	list_move_tail(&out->list, &c->busy_buffers);
	c9_log("%s: buffer = %p, data = %p, size = %d\n",
	       __func__, out, out->data, out->size);
	return out;
}

static void put_buffer(struct C9p_buffer *b)
{
	if (_maybe_init() < 0)
		return;
	c9_log("%s, buffer = %p, data = %p\n", __func__, b, b->data);
	list_move_tail(&b->list, &free_buffers);
}

uint8_t *default_begin(C9ctx *ctx, uint32_t size)
{
	struct C9p_connection *c = ctx->aux;
	struct C9p_buffer *b = get_buffer(c, size);

	if (!b)
		return NULL;
	return b->data;
}

/* Default end method: send enqueued messages for a connection */
int default_end(C9ctx *ctx)
{
	struct C9p_connection *c = ctx->aux;
	struct C9p_connection_host_data *d;
	struct C9p_buffer *b, *tmp;
	int out = 0;

	if (!c)
		return -1;
	d = c->data;
	list_for_each_entry_safe(b, tmp, &c->busy_buffers, list) {
		out = c9_send(d, b->data, b->size);
		if (out < 0) {
			c9_err("ERROR WRITING PACKET\n");
			out = -1;
		}
		put_buffer(b);
	}
	return out < 0 ? out : 0;
}

void free_connection(struct C9p_connection *c)
{
	if (_maybe_init() < 0)
		return;
	list_add_tail(&c->list, &free_connections);
}

void free_association(struct C9p_association *a)
{
	if (_maybe_init() < 0)
		return;
	list_add_tail(&a->list, &free_associations);
}
