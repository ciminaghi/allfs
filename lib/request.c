#include <c9-server.h>
#include <fid.h>
#include <tree.h>

#define ERROR_OVERHEAD (2 + 7)

static int _check_permissions(enum C9mode mode, const struct C9p_tree_el *e,
			      const struct C9p_association *a)
{
	struct C9stat s;

	if (!e->fops->stat || e->fops->stat(e, &s) < 0)
		return -1;
	/* FIXME: IMPLEMENT THIS */
	return 0;
}

void default_t(C9ctx *ctx, C9t *t)
{
	c9_log("%s invoked, t->type = %d\n", __func__, t->type);
	switch (t->type) {
	case Tauth:
		c9_log("AUTH !\n");
		/* No authentication required, return error */
		s9error(ctx, t->tag, "no auth required");
		return;
	case Tattach:
	{
		struct C9p_association *a;
		struct C9p_connection *c = ctx->aux;
		struct C9qid q;

		c9_log("ATTACH (%s) !\n", t->attach.aname);
		a = new_association(c, t->fid, t->attach.afid,
				    t->attach.aname,
				    t->attach.uname);
		if (!a) {
			c9_err("root not found !\n");
			s9error(ctx, t->tag, "root not found");
			return;
		}
		q.version = a->root->data->version;
		q.type = a->root->stat->qid.type;
		q.path = a->root->fops->get_path(a->root);
		if (s9attach(ctx, t->tag, &q)) {
			s9error(ctx, t->tag, "attach error");
			return;
		}
		return;
	}
	case Twalk:
	{
		struct C9p_connection *c = ctx->aux;
		struct C9p_association *a;
		const struct C9p_tree_el *e;
		struct C9qid qs[C9maxpathel];
		int nqs;
		
		c9_log("WALK !\n");

		memset(qs, 0, sizeof(*qs));
		e = traverse(c, t->fid, t->walk.wname, &nqs, qs, &a);
		if (!e) {
			s9error(ctx, t->tag, "unknown path");
			return;
		}
		if (add_fid(c, t->walk.newfid, e, a) < 0) {
			s9error(ctx, t->tag, "invalid fid 3");
			return;
		}
		if (s9walk(ctx, t->tag, nqs, qs)) {
			s9error(ctx, t->tag, "walk error");
			return;
		}
		return;
	}
	case Tstat:
	{
		struct C9p_connection *c = ctx->aux;
		struct C9stat s;
		struct fid_struct *f = find_fid(c, t->fid);
		const struct C9p_tree_el *e = NULL;

		c9_log("STAT !\n");
		if (f)
			e = f->te;
		if (!e) {
			s9error(ctx, t->tag, "invalid fid 4");
			return;
		}
		if (e->fops->stat(e, &s) < 0) {
			s9error(ctx, t->tag, "stat error");
			return;
		}
		if (s9stat(ctx, t->tag, &s)) {
			s9error(ctx, t->tag, "stat error");
			return;
		}
		return;
	}
	case Twstat:
		c9_log("WSTAT !\n");
		if (s9wstat(ctx, t->tag)) {
			s9error(ctx, t->tag, "wstat error");
			return;
		}
		return;
	case Tversion:
		c9_log("VERSION !\n");
		if (s9version(ctx)) {
			s9error(ctx, t->tag, "version error");
			return;
		}
		return;
	case Tclunk:
	{
		struct C9p_connection *c = ctx->aux;

		c9_log("CLUNK !\n");
		if (clunk_fid(c, t->fid) < 0) {
			s9error(ctx, t->tag, "invalid fid 5");
			return;
		}
		s9clunk(ctx, t->tag);
		return;
	}
	case Topen:
	{
		struct C9p_connection *c = ctx->aux;
		const struct C9p_association *a = NULL;		
		struct fid_struct *f = find_fid(c, t->fid);
		const struct C9p_tree_el *e = NULL;
		struct C9qid q;

		c9_log("OPEN !\n");
		if (f) {
			e = f->te;
			a = f->association;
		}
		if (!e || !a) {
			s9error(ctx, t->tag, "invalid fid 6");
			return;
		}
		if (_check_permissions(t->open.mode, e, a) < 0) {
			s9error(ctx, t->tag, "permission denied");
			return;
		}
		if (e->fops->open) {
			if (e->fops->open(e, t->open.mode) < 0) {
				s9error(ctx, t->tag, "open error");
				return;
			}
		}
		q.version = e->data->version;
		q.type = e->stat->qid.type;
		q.path = e->fops->get_path(e);
		s9open(ctx, t->tag, &q, 0);
		return;
	}
	case Tread:
	{
		struct C9p_connection *c = ctx->aux;
		struct fid_struct *f = find_fid(c, t->fid);
		const struct C9p_tree_el *e = NULL;
		/* FIXME !! */
		uint8_t buf[BUF_SIZE];
		uint32_t count = min(sizeof(buf), t->read.size);

		c9_log("READ !\n");
		if (f)
			e = f->te;
		if (!e) {
			s9error(ctx, t->tag, "invalid fid 1");
			return;
		}
		if (!e->fops->read) {
			s9error(ctx, t->tag, "read not implemented");
			return;
		}
		if ((e->stat->qid.type & C9qtdir) &&
		    (f->seekptr != t->read.offset)) {
			s9error(ctx, t->tag, "invalid offset");
			return;
		}
		count = e->fops->read(e, t->read.offset, count, buf);
		c9_log("%u bytes read\n", count);
		if (!(e->stat->qid.type & C9qtdir))
			/* Regular file */
			s9read(ctx, t->tag, buf, count);
		else {
			int i, n;
			const struct C9stat *dentries[MAX_DENTRIES];

			/* Directory */
			if (f->seekptr != t->read.offset) {
				s9error(ctx, t->tag, "invalid offset");
				return;
			}
			if (count % sizeof(struct C9stat)) {
				s9error(ctx, t->tag,
					"internal error reading dir");
				return;
			}
			n = count / sizeof(struct C9stat);
			for (i = 0; i < n; i++)
				dentries[i] = &((struct C9stat *)buf)[i];
			c9_log("%s: %d dentries\n", __func__, n);
			if (s9readdir(ctx, t->tag, dentries,
				      &n, &f->seekptr, count)) {
				s9error(ctx, t->tag, "s9readdir error");
				return;
			}
		}
		return;

	}
	case Twrite:
	{
		struct C9p_connection *c = ctx->aux;
		struct fid_struct *f = find_fid(c, t->fid);
		const struct C9p_tree_el *e = NULL;
		int count;

		c9_log("READ !\n");
		if (f)
			e = f->te;
		if (!e) {
			s9error(ctx, t->tag, "invalid fid 2");
			return;
		}
		if (!e->fops->write) {
			s9error(ctx, t->tag, "write not implemented");
			return;
		}
		count = e->fops->write(e, t->write.offset, t->write.size,
				       t->write.data);
		c9_log("%u bytes written\n", count);
		s9write(ctx, t->tag, count);
		return;

	}
	default:
		c9_log("UNKNOWN (%d)\n", t->type);
		return;
	}
	return;
}
