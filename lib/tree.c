#include <c9-server.h>
#include <fid.h>

const struct C9p_tree_el *find_root(const char *name)
{
	const struct C9p_tree_el *ptr;

	c9_log("%s(%s)\n", __func__, name);
	for (ptr = roots_start; ptr != roots_end; ptr++) {
		c9_log("candidate %s\n", ptr->stat->name);
		if (!strcmp(name, ptr->stat->name))
			return ptr;
	}
	return NULL;
}

static const struct C9p_tree_el *
_look_for_child(const struct C9p_children_data *cd,
		char *n)
{
	int i;

	for (i = 0; i < cd->nchildren; i++)
		if (!strcmp(cd->children[i]->stat->name, n))
			return cd->children[i];
	return NULL;
}

static inline void _set_qid(struct C9qid *dst, const struct C9p_tree_el *te)
{
	*dst = te->stat->qid;
	/* Override qid version with runtime data */
	dst->version = te->data->version;
}

const struct C9p_tree_el *traverse(struct C9p_connection *c,
				   uint32_t start_fid,
				   char *wname[],
				   int *nqs,
				   struct C9qid qs[],
				   const struct C9p_association **a)
{
	const struct C9p_tree_el *out = NULL;
	struct fid_struct *f = find_fid(c, start_fid);
	int i;
	const struct C9p_children_data *cd;

	if (!f)
		return out;
	
	out = f->te;
	*a = f->association;
	*nqs = 0;

	c9_log("%s: out = %p\n", __func__, out);
	memset(qs, 0, sizeof(*qs) * C9maxpathel);
	for (i = 0; i < C9maxpathel && out && wname[i]; i++) {
		struct C9qid *q = &qs[i];

		c9_log("%s: wname[%d] = %s\n", __func__, i, wname[i]);
		if (!strcmp(wname[i], ".")) {
			_set_qid(q, out);
			continue;
		}
		if (!strcmp(wname[i], "..")) {
			out = out->parent;
			_set_qid(q, out);
			continue;
		}
		/* First look for wname[i] in static children */
		cd = out->static_children_data;
		if (cd) {
			out = _look_for_child(cd, wname[i]);
			if (out) {
				_set_qid(q, out);
				continue;
			}
		}
		/* Not found, look in runtime children, if any */
		cd = out->data->runtime_children_data;
		if (!cd)
			return out;
		out = _look_for_child(cd, wname[i]);
		if (out)
			_set_qid(q, out);
	}
	*nqs = i;
	return out;
}

int generic_stat(const struct C9p_tree_el *e, struct C9stat *s)
{
	*s = *e->stat;
	s->qid.version = e->data->version;
	return sizeof(*s);
}
