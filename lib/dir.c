/* directory fops implementation */

#include <c9-server.h>
#include <tree.h>

static int _dir_read(const struct C9p_children_data *cd,
		     int child_offset,
		     uint32_t count, struct C9stat *s)
{
	int i, out, stat;

	c9_log("%s: nchildren = %d, child_offset = %d\n", __func__,
	       cd->nchildren, child_offset);
	for (i = child_offset, out = 0;
	     i < cd->nchildren && out <= (count - (sizeof(*s)));
	     i++, s++, out += stat) {
		const struct C9p_tree_el *ce = cd->children[i];

		if (!ce)
			return -1;
		c9_log("%s, ce->name = %s\n", __func__, ce->stat->name);
		stat = generic_stat(ce, s);
		if (stat < 0)
			return stat;
	}
	return out;
}

int dir_read(const struct C9p_tree_el *e, uint64_t offset,
	     uint32_t count, uint8_t *data)
{
	struct C9stat *s = (struct C9stat *)data;
	const struct C9p_children_data *cd = e->static_children_data;
	int stat, out = 0, child_offset;

	child_offset = offset / sizeof(*s);

	if (child_offset < cd->nchildren) {
		stat = _dir_read(cd, child_offset, count, s);
		if (stat < 0 || stat >= count)
			return stat;
		out += stat;
		data += stat;
	}
	child_offset -= cd->nchildren;
	if (child_offset < 0)
		child_offset = 0;
	cd = e->data->runtime_children_data;
	if (!cd)
		return out;
	if (child_offset >= cd->nchildren)
		return out;
	s = (struct C9stat *)data;
	stat = _dir_read(cd, child_offset, count - out, s);
	if (stat < 0)
		return stat;
	return out + stat;
}
