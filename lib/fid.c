
#include <c9.h>
#include <fid.h>
#include <list.h>
#include <stdint.h>
#include <stdio.h>


#ifndef MAX_FIDS
#define MAX_FIDS 128
#endif

static struct fid_struct fids[MAX_FIDS];

static struct list_head free_fids;

static unsigned _hash(uint32_t v)
{
	return v % FIDS_TABLE_SIZE;
}

static inline int _bucket_initialized(struct C9p_connection *c, unsigned i)
{
	return c->fids_table[i].next || c->fids_table[i].prev;
}

struct fid_struct *find_fid(struct C9p_connection *c, uint32_t fid)
{
	struct fid_struct *out = NULL, *f;
	unsigned i = _hash(fid);

	c9_log("%s, fid = %u\n", __func__, fid);
	if (!_bucket_initialized(c, i))
		return out;
	list_for_each_entry(f, &c->fids_table[i], list) {
		c9_log("%s, candidate fid %p, fid = %u\n", __func__, f, f->fid);
		if (f->fid == fid) {
			out = f;
			break;
		}
	}
	c9_log("%s returns %p\n", __func__, out);
	return out;
}

static void _init_free_fids(void)
{
	int i;

	INIT_LIST_HEAD(&free_fids);
	for (i = 0; i < MAX_FIDS; i++)
		list_add(&fids[i].list, &free_fids);
}

static struct fid_struct *_new_fid(uint32_t fid)
{
	struct fid_struct *out = NULL;

	if (!free_fids.next && !free_fids.prev)
		_init_free_fids();
	if (list_empty(&free_fids))
		return out;
	out = list_entry(free_fids.next, struct fid_struct, list);
	out->fid = fid;
	list_del(&out->list);
	return out;
}

int add_fid(struct C9p_connection *c, uint32_t fid, const struct C9p_tree_el *e,
	    const struct C9p_association *a)
{
	unsigned i = _hash(fid);
	struct fid_struct *f;

	if (!_bucket_initialized(c, i))
		INIT_LIST_HEAD(&c->fids_table[i]);
	f = find_fid(c, fid);
	if (f) {
		c9_log("%s FILE %u IS ALREADY THERE, e = %p, f->te = %p\n",
		       __func__, fid, e, f->te);
		return (e == f->te) ? 0 : -1;
	}
	f = _new_fid(fid);
	f->te = e;
	f->association = a;
	f->seekptr  = 0;
	list_add_tail(&f->list, &c->fids_table[i]);
	c9_log("%s: added fid %p(%u), bucket = %u\n", __func__, f, fid, i);
	return 0;
}

int clunk_fid(struct C9p_connection *c, uint32_t fid)
{
	struct fid_struct *f = find_fid(c, fid);

	if (!f)
		return -1;
	list_move(&f->list, &free_fids);
	return 0;
}
